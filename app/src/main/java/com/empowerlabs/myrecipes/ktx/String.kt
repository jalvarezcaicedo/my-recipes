package com.empowerlabs.myrecipes.ktx

fun String.parseToViewId() = this.padStart(3, '0')