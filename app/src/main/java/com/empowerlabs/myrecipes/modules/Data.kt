package com.empowerlabs.myrecipes.modules

import android.app.Application
import com.empowerlabs.data.repository.RecipeDetailRepository
import com.empowerlabs.data.repository.RecipeRepository
import com.empowerlabs.data.source.LocalRecipeDataSource
import com.empowerlabs.data.source.LocalRecipeInfoDataSource
import com.empowerlabs.data.source.RemoteRecipeDataSource
import com.empowerlabs.data.source.RemoteRecipeInfoDataSource
import com.empowerlabs.myrecipes.business.database.RecipesDB
import com.empowerlabs.myrecipes.business.database.source.LocalRecipeDataSourceImpl
import com.empowerlabs.myrecipes.business.database.source.LocalRecipeInfoDataSourceImpl
import com.empowerlabs.myrecipes.business.server.service.RecipeApiService
import com.empowerlabs.myrecipes.business.server.source.RemoteRecipeDataSourceImpl
import com.empowerlabs.myrecipes.business.server.source.RemoteRecipeInfoDataSourceImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class Data {

    @Provides
    @Singleton
    fun databaseProvider(app: Application): RecipesDB = RecipesDB.getDatabase(app)

    @Provides
    fun recipeRepository(
        localRecipeDataSource: LocalRecipeDataSource, remoteRecipeDataSource: RemoteRecipeDataSource
    ) = RecipeRepository(localRecipeDataSource, remoteRecipeDataSource)

    @Provides
    fun recipeDetailRepository(
        localRecipeInfoDataSource: LocalRecipeInfoDataSource,
        remoteRecipeInfoDataSource: RemoteRecipeInfoDataSource
    ) = RecipeDetailRepository(localRecipeInfoDataSource, remoteRecipeInfoDataSource)

    @Provides
    fun localRecipeDataSourceProvider(
        db: RecipesDB
    ): LocalRecipeDataSource = LocalRecipeDataSourceImpl(db)

    @Provides
    fun localRecipeInfoDataSourceProvider(
        db: RecipesDB
    ): LocalRecipeInfoDataSource = LocalRecipeInfoDataSourceImpl(db)

    @Provides
    fun remoteRecipeDataSourceProvider(
        recipeApiService: RecipeApiService
    ): RemoteRecipeDataSource = RemoteRecipeDataSourceImpl(recipeApiService)

    @Provides
    fun remoteRecipeInfoDataSourceProvider(
        recipeApiService: RecipeApiService
    ): RemoteRecipeInfoDataSource = RemoteRecipeInfoDataSourceImpl(recipeApiService)

}