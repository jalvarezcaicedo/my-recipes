package com.empowerlabs.myrecipes.business.server.source

import arrow.core.Either
import com.empowerlabs.data.source.RemoteRecipeDataSource
import com.empowerlabs.domain.RecipeDomain
import com.empowerlabs.myrecipes.BuildConfig
import com.empowerlabs.myrecipes.business.server.service.RecipeApiService
import com.empowerlabs.myrecipes.business.server.toRecipeDomain
import com.empowerlabs.myrecipes.business.util.tryCall

class RemoteRecipeDataSourceImpl(
    private val recipeApiService: RecipeApiService
) : RemoteRecipeDataSource {


    override suspend fun getRecipes(limit: Long): Either<com.empowerlabs.domain.Error, List<RecipeDomain>> =
        tryCall {
            recipeApiService.getRandomRecipe(BuildConfig.API_KEY, true, limit).recipes.map {
                it.toRecipeDomain()
            }
        }


}