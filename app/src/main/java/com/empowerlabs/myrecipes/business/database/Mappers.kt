package com.empowerlabs.myrecipes.business.database

import com.empowerlabs.domain.RecipeDomain
import com.empowerlabs.domain.RecipeInfoDomain
import com.empowerlabs.myrecipes.business.database.entity.RecipeEntity
import com.empowerlabs.myrecipes.business.database.entity.RecipeInfoEntity


fun RecipeDomain.toRecipeEntity() = RecipeEntity(
    id, title, image, sourceName, 0
)

fun RecipeEntity.toRecipeDomain() = RecipeDomain(
    id, title, image, sourceName, favorite
)

fun List<RecipeEntity>.toRecipeDomainList(): List<RecipeDomain> = map {
    it.toRecipeDomain()
}

fun RecipeInfoDomain.toRecipeInfoEntity() = RecipeInfoEntity(
    id, title, image, creditsText, summary
)

fun RecipeInfoEntity.toRecipeInfoDomain() = RecipeInfoDomain(
    id, title, image, creditsText, summary
)