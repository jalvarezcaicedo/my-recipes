package com.empowerlabs.myrecipes.business.server.response

data class RecipeInfoServer(
    var id: Long,
    var title: String,
    var image: String,
    var creditsText: String,
    var summary: String
)
