package com.empowerlabs.myrecipes.business.server.response

data class RecipesServer(
    var recipes: List<RecipeServer>
)

data class RecipeServer(
    var id: Long, var title: String, var image: String, var sourceName: String
)
