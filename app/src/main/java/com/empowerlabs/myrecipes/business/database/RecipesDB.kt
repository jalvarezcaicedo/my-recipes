package com.empowerlabs.myrecipes.business.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.empowerlabs.myrecipes.business.database.dao.RecipeDao
import com.empowerlabs.myrecipes.business.database.dao.RecipeInfoDao
import com.empowerlabs.myrecipes.business.database.entity.RecipeEntity
import com.empowerlabs.myrecipes.business.database.entity.RecipeInfoEntity

@Database(
    entities = [
        RecipeEntity::class,
        RecipeInfoEntity::class
    ],
    version = 1,
    exportSchema = false
)
abstract class RecipesDB : RoomDatabase() {

    abstract fun recipeDao(): RecipeDao

    abstract fun recipeInfoDao(): RecipeInfoDao

    companion object {
        @Synchronized
        fun getDatabase(context: Context): RecipesDB = Room.databaseBuilder(
            context.applicationContext, RecipesDB::class.java, "recipes_db"
        ).build()
    }

}