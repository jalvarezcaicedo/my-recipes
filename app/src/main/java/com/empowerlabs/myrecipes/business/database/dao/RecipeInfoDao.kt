package com.empowerlabs.myrecipes.business.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.empowerlabs.myrecipes.business.database.entity.RecipeInfoEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface RecipeInfoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRecipeInfo(recipeInfo: RecipeInfoEntity)

    @Query("SELECT * FROM RecipeInfo WHERE id = :id")
    fun getRecipeInfo(id: Long): Flow<RecipeInfoEntity>
}