package com.empowerlabs.myrecipes.business.server.source

import arrow.core.Either
import com.empowerlabs.data.source.RemoteRecipeInfoDataSource
import com.empowerlabs.domain.Error
import com.empowerlabs.domain.RecipeInfoDomain
import com.empowerlabs.myrecipes.BuildConfig
import com.empowerlabs.myrecipes.business.server.service.RecipeApiService
import com.empowerlabs.myrecipes.business.server.toRecipeInfoDomain
import com.empowerlabs.myrecipes.business.util.tryCall

class RemoteRecipeInfoDataSourceImpl(
    private val recipeApiService: RecipeApiService
) : RemoteRecipeInfoDataSource {

    override suspend fun getRecipeInfo(id: Long): Either<Error, RecipeInfoDomain> = tryCall {
        recipeApiService.getRecipeInformation(id.toString(), BuildConfig.API_KEY)
            .toRecipeInfoDomain()
    }


}