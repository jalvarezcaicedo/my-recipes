package com.empowerlabs.myrecipes.business.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.empowerlabs.myrecipes.business.database.entity.RecipeEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface RecipeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(recipeData: List<RecipeEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRecipe(recipe: RecipeEntity)

    @Query("SELECT * FROM Recipe ORDER BY favorite DESC")
    fun findRecipes(): Flow<List<RecipeEntity>>

    @Query("SELECT * FROM Recipe WHERE title LIKE '%'||:query||'%' ORDER BY favorite DESC")
    fun findRecipeFiltered(query: String): Flow<List<RecipeEntity>>

    @Query("UPDATE Recipe SET favorite =:favorite WHERE id = :idRecipe")
    suspend fun updateRecipeFavorite(idRecipe: String, favorite: Int)

}