package com.empowerlabs.myrecipes.business.server.service

import com.empowerlabs.myrecipes.business.server.response.RecipeInfoServer
import com.empowerlabs.myrecipes.business.server.response.RecipesServer
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RecipeApiService {

    @GET("recipes/random")
    suspend fun getRandomRecipe(
        @Query("apiKey") apiKey: String,
        @Query("limitLicense") license: Boolean,
        @Query("number") limit: Long
    ): RecipesServer

    @GET("/recipes/{id}/information")
    suspend fun getRecipeInformation(
        @Path("id") id: String, @Query("apiKey") apiKey: String
    ): RecipeInfoServer

}