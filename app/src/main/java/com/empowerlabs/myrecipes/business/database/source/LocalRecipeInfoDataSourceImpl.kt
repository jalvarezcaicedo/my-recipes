package com.empowerlabs.myrecipes.business.database.source

import com.empowerlabs.data.source.LocalRecipeInfoDataSource
import com.empowerlabs.domain.RecipeInfoDomain
import com.empowerlabs.myrecipes.business.database.RecipesDB
import com.empowerlabs.myrecipes.business.database.toRecipeInfoDomain
import com.empowerlabs.myrecipes.business.database.toRecipeInfoEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class LocalRecipeInfoDataSourceImpl(
    private val db: RecipesDB
) : LocalRecipeInfoDataSource {

    private val recipeInfoDao by lazy { db.recipeInfoDao() }

    override fun getLocalRecipeInfoData(id: Long): Flow<RecipeInfoDomain> =
        recipeInfoDao.getRecipeInfo(id).map { it.toRecipeInfoDomain() }


    override suspend fun saveRecipeInfo(recipeInfo: RecipeInfoDomain) {
        recipeInfoDao.insertRecipeInfo(recipeInfo.toRecipeInfoEntity())
    }
}