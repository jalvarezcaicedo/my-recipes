package com.empowerlabs.myrecipes.business.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "RecipeInfo")
data class RecipeInfoEntity(
    @PrimaryKey
    var id: Long,
    var title: String,
    var image: String,
    var creditsText: String,
    var summary: String
)
