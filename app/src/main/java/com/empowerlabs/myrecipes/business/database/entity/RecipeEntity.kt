package com.empowerlabs.myrecipes.business.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Recipe")
data class RecipeEntity(
    @PrimaryKey var id: Long,
    var title: String,
    var image: String?,
    var sourceName: String,
    var favorite: Int
)
