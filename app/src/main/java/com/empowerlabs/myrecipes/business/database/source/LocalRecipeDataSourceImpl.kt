package com.empowerlabs.myrecipes.business.database.source

import com.empowerlabs.data.source.LocalRecipeDataSource
import com.empowerlabs.domain.RecipeDomain
import com.empowerlabs.myrecipes.business.database.RecipesDB
import com.empowerlabs.myrecipes.business.database.toRecipeDomainList
import com.empowerlabs.myrecipes.business.database.toRecipeEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class LocalRecipeDataSourceImpl(
    private val db: RecipesDB
) : LocalRecipeDataSource {

    private val recipesDao by lazy { db.recipeDao() }
    override fun getLocalRecipesData(): Flow<List<RecipeDomain>> = recipesDao.findRecipes().map {
        it.toRecipeDomainList()
    }

    override fun getLocalRecipesDataFiltered(query: String): Flow<List<RecipeDomain>> =
        recipesDao.findRecipeFiltered(query).map {
            it.toRecipeDomainList()
        }

    override suspend fun saveRecipes(recipes: List<RecipeDomain>) {
        recipesDao.insertAll(recipes.map { it.toRecipeEntity() })
    }

    override suspend fun updateRecipeState(idRecipe: String, favorite: Int) {
        recipesDao.updateRecipeFavorite(idRecipe, favorite)
    }

}