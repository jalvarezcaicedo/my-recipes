package com.empowerlabs.myrecipes.business.server

import com.empowerlabs.domain.RecipeDomain
import com.empowerlabs.domain.RecipeInfoDomain
import com.empowerlabs.myrecipes.business.server.response.RecipeInfoServer
import com.empowerlabs.myrecipes.business.server.response.RecipeServer

fun RecipeServer.toRecipeDomain() = RecipeDomain(
    id, title, image, sourceName, 0
)

fun RecipeInfoServer.toRecipeInfoDomain() = RecipeInfoDomain(
    id, title, image, creditsText, summary
)