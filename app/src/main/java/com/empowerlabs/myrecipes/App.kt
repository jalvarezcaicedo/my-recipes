package com.empowerlabs.myrecipes

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class App : Application() {

    companion object {
        private var instance: App? = null

        @JvmStatic
        @Synchronized
        fun getInstance(): App = instance!!
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

}