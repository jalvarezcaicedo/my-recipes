package com.empowerlabs.myrecipes.feature.recipes

import com.empowerlabs.domain.RecipeDomain

interface RecipeItemListener {

    fun onSelectRecipeListener(recipeDomain: RecipeDomain)

    fun onFavoriteSelected(recipeDomain: RecipeDomain, favorite: Int)
}