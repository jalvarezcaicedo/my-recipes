package com.empowerlabs.myrecipes.feature.recipes

import androidx.appcompat.widget.SearchView.OnQueryTextListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.empowerlabs.domain.RecipeDomain
import com.empowerlabs.myrecipes.databinding.FragmentRecipeBinding
import com.empowerlabs.myrecipes.feature.common.BaseFragment
import com.empowerlabs.myrecipes.ktx.hideKeyboard
import com.empowerlabs.myrecipes.ktx.launchAndCollect
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import timber.log.Timber

@AndroidEntryPoint
class RecipeFragment : BaseFragment<FragmentRecipeBinding>(), OnQueryTextListener,
    RecipeItemListener {

    private val recipeAdapter: RecipeAdapter = RecipeAdapter(this)
    private val recipeViewModel: RecipeViewModel by viewModels()
    private val recipeData = mutableListOf<RecipeDomain>()

    override fun initView() {
        binding.rvRecipes.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(this@RecipeFragment.requireContext(), 3)
            adapter = recipeAdapter
        }
    }

    override fun initListeners() {
        binding.svRecipeFinder.setOnQueryTextListener(this)
    }

    override fun initObservables() {
        with(recipeViewModel.state) {
            diff({ it.loading }) { handleVisibility(it, listOf()) }
            diff({ it.remoteRecipes }) {
                it?.let { recipeViewModel.saveRecipeData(it) }
            }
            diff({ it.recipes }) {
                it?.let {
                    recipeData.clear()
                    recipeData.addAll(it)
                    binding.svRecipeFinder.setQuery("", false)
                    binding.svRecipeFinder.clearFocus()
                    handleUpdateRecipes(it)
                }
            }
            diff({ it.recipesFiltered }) {
                it?.let {
                    recipeData.clear()
                    recipeData.addAll(it)
                    handleUpdateRecipes(it)
                }
            }
            diff({ it.error }) {
                it?.let {
                    Snackbar.make(binding.clRecipes, it.toString(), Snackbar.LENGTH_SHORT).show()
                }
            }
        }

        recipeViewModel.getRecipeDataServer()
        recipeViewModel.getLocalRecipes()
    }

    override fun getParametersFragment() {}

    override fun getViewBinding(): FragmentRecipeBinding =
        FragmentRecipeBinding.inflate(layoutInflater)

    private fun handleVisibility(loading: Boolean, data: List<RecipeDomain>) {
        Timber.d(loading.toString().plus(" $data"))
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        hideKeyboard()
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        if (newText.isNullOrBlank()) recipeViewModel.getLocalRecipes()
        else recipeViewModel.getRecipesFiltered(newText)

        return true
    }

    override fun onSelectRecipeListener(recipeDomain: RecipeDomain) {
        val navAction = RecipeFragmentDirections.actionMenuRecipesToRecipeDetail(recipeDomain.id)
        findNavController().navigate(navAction)
    }

    override fun onFavoriteSelected(recipeDomain: RecipeDomain, favorite: Int) {
        recipeViewModel.updateRecipeStatus(recipeDomain, favorite)
    }

    private fun handleUpdateRecipes(recipes: List<RecipeDomain>) {
        handleVisibility(false, recipes)
        recipeAdapter.submitRecipesData(recipes)
        binding.rvRecipes.smoothScrollToPosition(0)
    }

    private fun <T, U> Flow<T>.diff(mapFlow: (T) -> U, body: (U) -> Unit) {
        viewLifecycleOwner.launchAndCollect(
            flow = map(mapFlow).distinctUntilChanged(), body = body
        )
    }

}