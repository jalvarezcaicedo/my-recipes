package com.empowerlabs.myrecipes.feature.recipes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.empowerlabs.domain.RecipeDomain
import com.empowerlabs.myrecipes.R
import com.empowerlabs.myrecipes.databinding.ItemRecipeBinding

class RecipeAdapter(
    private val listener: RecipeItemListener
) : RecyclerView.Adapter<RecipeAdapter.RecipeGridViewHolder>() {

    private val differCallback = object : DiffUtil.ItemCallback<RecipeDomain>() {
        override fun areItemsTheSame(oldItem: RecipeDomain, newItem: RecipeDomain): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: RecipeDomain, newItem: RecipeDomain): Boolean {
            return oldItem == newItem
        }
    }

    private val differ = AsyncListDiffer(this, differCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeGridViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_recipe, parent, false)
        return RecipeGridViewHolder(view)
    }

    override fun getItemCount(): Int = differ.currentList.size

    fun submitRecipesData(recipes: List<RecipeDomain>) {
        differ.submitList(recipes)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecipeGridViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

    inner class RecipeGridViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        private val binding = ItemRecipeBinding.bind(view)

        fun bind(item: RecipeDomain) {
            binding.clRecipeItem.setOnClickListener {
                listener.onSelectRecipeListener(item)
            }
            binding.cbFavorite.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) listener.onFavoriteSelected(item, 1)
                else listener.onFavoriteSelected(item, 0)
            }

            if (item.image.isNullOrBlank()) {
                item.image.let {
                    Glide.with(view).load(
                        ContextCompat.getDrawable(
                            binding.root.context, R.drawable.image_not_found_icon
                        )
                    ).centerCrop().into(binding.ivRecipeImage)
                }
            } else {
                item.image.let {
                    Glide.with(view).load(
                        item.image
                    ).centerCrop().into(binding.ivRecipeImage)
                }
            }

            binding.tvRecipeTitle.text = item.title
            binding.tvRecipeSource.text = item.sourceName
            binding.cbFavorite.isChecked = item.favorite == 1
        }
    }
}