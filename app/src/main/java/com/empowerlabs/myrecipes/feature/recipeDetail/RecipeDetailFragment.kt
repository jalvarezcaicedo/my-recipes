package com.empowerlabs.myrecipes.feature.recipeDetail

import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.empowerlabs.domain.RecipeInfoDomain
import com.empowerlabs.myrecipes.databinding.FragmentRecipeDetailBinding
import com.empowerlabs.myrecipes.feature.common.BaseFragment
import com.empowerlabs.myrecipes.ktx.launchAndCollect
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import timber.log.Timber

@AndroidEntryPoint
class RecipeDetailFragment : BaseFragment<FragmentRecipeDetailBinding>() {

    private val viewModel: RecipeDetailViewModel by viewModels()
    private val safeArgs: RecipeDetailFragmentArgs by navArgs()
    private var recipeId: Long = 1L


    override fun initView() {}

    override fun initListeners() {
        binding.ivBackArrow.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    override fun initObservables() {
        with(viewModel.state) {
            diff({ it.loading }) { handleVisibility(it, RecipeInfoDomain(1, "t", "t", "t", "t")) }
            diff({ it.remoteRecipeInfo }) {
                it?.let {
                    viewModel.getLocalRecipeDetail(it.id)
                }
            }
            diff({ it.localRecipeInfo }) {
                it?.let {
                    updateUI(it)
                }
            }
            diff({ it.error }) {
                it?.let {
                    Snackbar.make(binding.clRecipeDetail, it.toString(), Snackbar.LENGTH_SHORT)
                        .show()
                }
            }
        }

        viewModel.getRemoteRecipeDetail(recipeId)
        viewModel.getLocalRecipeDetail(recipeId)
    }

    override fun getParametersFragment() {
        recipeId = safeArgs.recipeId
    }

    override fun getViewBinding(): FragmentRecipeDetailBinding =
        FragmentRecipeDetailBinding.inflate(layoutInflater)

    private fun updateUI(recipeInfoDomain: RecipeInfoDomain) {
        Glide.with(requireContext()).load(
            recipeInfoDomain.image
        ).centerCrop().into(binding.ivRecipeImage)

        binding.tvRecipeTitle.text = recipeInfoDomain.title
        binding.tvRecipeCredits.text = recipeInfoDomain.creditsText
        binding.tvRecipeInstructions.text = recipeInfoDomain.summary
    }


    private fun handleVisibility(loading: Boolean, data: RecipeInfoDomain) {
        Timber.d(loading.toString().plus(" $data"))
    }

    private fun <T, U> Flow<T>.diff(mapFlow: (T) -> U, body: (U) -> Unit) {
        viewLifecycleOwner.launchAndCollect(
            flow = map(mapFlow).distinctUntilChanged(), body = body
        )
    }

}