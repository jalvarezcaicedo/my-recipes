package com.empowerlabs.myrecipes.feature.recipeDetail

import com.empowerlabs.data.repository.RecipeDetailRepository
import com.empowerlabs.usecase.recipeDetail.GetLocalRecipeDetailUseCase
import com.empowerlabs.usecase.recipeDetail.GetRemoteRecipeDetailUseCase
import com.empowerlabs.usecase.recipeDetail.SaveRecipeDetailUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
class RecipeDetailModule {

    @Provides
    @ViewModelScoped
    fun saveRecipeInfoUseCase(
        recipeDetailRepository: RecipeDetailRepository
    ) = SaveRecipeDetailUseCase(recipeDetailRepository)

    @Provides
    @ViewModelScoped
    fun getLocalRecipeInfoUseCase(
        recipeDetailRepository: RecipeDetailRepository
    ) = GetLocalRecipeDetailUseCase(recipeDetailRepository)

    @Provides
    @ViewModelScoped
    fun getRemoteRecipeInfoUseCase(
        recipeDetailRepository: RecipeDetailRepository
    ) = GetRemoteRecipeDetailUseCase(recipeDetailRepository)

}