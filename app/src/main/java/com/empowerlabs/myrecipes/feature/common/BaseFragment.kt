package com.empowerlabs.myrecipes.feature.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.viewbinding.ViewBinding

abstract class BaseFragment<VB : ViewBinding> : Fragment() {

    private var _binding: VB? = null
    val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        _binding = getViewBinding()
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getParametersFragment()
        initView()
        initListeners()
        initObservables()
    }

    abstract fun initView()

    abstract fun initListeners()

    abstract fun initObservables()

    abstract fun getParametersFragment()

    abstract fun getViewBinding(): VB

    fun addFragmentWithAnim(
        fragment: Fragment, @IdRes containerFragment: Int, nameFragment: String
    ) {
        val transaction: FragmentTransaction = childFragmentManager.beginTransaction()
        transaction.replace(containerFragment, fragment)
        transaction.addToBackStack(nameFragment)
        transaction.commit()
    }

    fun attachFragment(fragment: Fragment, container: Int, tag: String) {
        childFragmentManager.beginTransaction().add(container, fragment, tag).addToBackStack(null)
            .commit()
    }
}
