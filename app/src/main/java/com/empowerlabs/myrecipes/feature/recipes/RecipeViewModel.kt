package com.empowerlabs.myrecipes.feature.recipes

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.empowerlabs.domain.Error
import com.empowerlabs.domain.RecipeDomain
import com.empowerlabs.usecase.recipe.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RecipeViewModel @Inject constructor(
    private val saveRecipeUseCase: SaveRecipeUseCase,
    private val updateRecipeStatusUseCase: UpdateRecipeStatusUseCase,
    private val getLocalRecipesUseCase: GetLocalRecipesUseCase,
    private val getLocalRecipesFilteredUseCase: GetLocalRecipesFilteredUseCase,
    private val getRemoteRecipeUseCase: GetRemoteRecipeUseCase
) : ViewModel() {

    private val _state = MutableStateFlow(UIState())
    val state: StateFlow<UIState> = _state.asStateFlow()

    fun saveRecipeData(recipes: List<RecipeDomain>) = viewModelScope.launch {
        saveRecipeUseCase.invoke(recipes)
    }

    fun updateRecipeStatus(recipe: RecipeDomain, favorite: Int) = viewModelScope.launch {
        updateRecipeStatusUseCase.invoke(recipe, favorite)
    }

    fun getRecipeDataServer() = viewModelScope.launch {
        _state.update { UIState(loading = true) }
        getRemoteRecipeUseCase().fold(ifLeft = { error ->
            _state.update { it.copy(error = error, loading = false) }
        }, ifRight = { response ->
            _state.update { it.copy(remoteRecipes = response, loading = false) }
        })
    }

    fun getLocalRecipes() = viewModelScope.launch {
        _state.update { UIState(loading = true) }
        getLocalRecipesUseCase().catch { }.collect { recipes ->
            _state.update { UIState(recipes = recipes) }
        }
    }

    fun getRecipesFiltered(query: String) = viewModelScope.launch {
        _state.update { UIState(loading = true) }
        getLocalRecipesFilteredUseCase(query).catch { }.collect { recipes ->
            _state.update { UIState(recipesFiltered = recipes) }
        }
    }

    data class UIState(
        val error: Error? = null,
        val loading: Boolean = false,
        val remoteRecipes: List<RecipeDomain>? = null,
        val recipes: List<RecipeDomain>? = null,
        val recipesFiltered: List<RecipeDomain>? = null
    )

}