package com.empowerlabs.myrecipes.feature.recipeDetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.empowerlabs.domain.Error
import com.empowerlabs.domain.RecipeInfoDomain
import com.empowerlabs.usecase.recipeDetail.GetLocalRecipeDetailUseCase
import com.empowerlabs.usecase.recipeDetail.GetRemoteRecipeDetailUseCase
import com.empowerlabs.usecase.recipeDetail.SaveRecipeDetailUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RecipeDetailViewModel @Inject constructor(
    private val getRemoteRecipeDetailUseCase: GetRemoteRecipeDetailUseCase,
    private val getLocalRecipeDetailUseCase: GetLocalRecipeDetailUseCase,
    private val saveRecipeDetailUseCase: SaveRecipeDetailUseCase

) : ViewModel() {

    private val _state = MutableStateFlow(UIState())
    val state: StateFlow<UIState> = _state.asStateFlow()

    fun getRemoteRecipeDetail(id: Long) =
        viewModelScope.launch {
            _state.update { UIState(loading = true) }
            getRemoteRecipeDetailUseCase(id).fold(
                ifLeft = { error ->
                    _state.update { it.copy(error = error, loading = false) }
                },
                ifRight = { response ->
                    saveRecipeDetailUseCase(response)
                    _state.update { it.copy(remoteRecipeInfo = response, loading = false) }
                }
            )
        }

    fun getLocalRecipeDetail(id: Long) = viewModelScope.launch {
        _state.update { UIState(loading = true) }
        getLocalRecipeDetailUseCase(id).catch {}.collect { recipeDetail ->
            _state.update { UIState(localRecipeInfo = recipeDetail, loading = false) }
        }
    }

    private fun saveRecipeInfoData(recipeInfoDomain: RecipeInfoDomain) = viewModelScope.launch {
        saveRecipeDetailUseCase(recipeInfoDomain)
    }


    data class UIState(
        val error: Error? = null,
        val loading: Boolean = false,
        val localRecipeInfo: RecipeInfoDomain? = null,
        val remoteRecipeInfo: RecipeInfoDomain? = null
    )

}