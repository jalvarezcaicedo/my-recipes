package com.empowerlabs.myrecipes.feature.recipes

import com.empowerlabs.data.repository.RecipeRepository
import com.empowerlabs.usecase.recipe.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
class RecipesModule {

    @Provides
    @ViewModelScoped
    fun saveRecipeUseCase(
        recipeRepository: RecipeRepository
    ) = SaveRecipeUseCase(recipeRepository)

    @Provides
    @ViewModelScoped
    fun updateRecipeUseCase(
        recipeRepository: RecipeRepository
    ) = UpdateRecipeStatusUseCase(recipeRepository)

    @Provides
    @ViewModelScoped
    fun getRemoteRecipeUseCase(
        recipeRepository: RecipeRepository
    ) = GetRemoteRecipeUseCase(recipeRepository)

    @Provides
    @ViewModelScoped
    fun getLocalRecipesUseCase(
        recipeRepository: RecipeRepository
    ) = GetLocalRecipesUseCase(recipeRepository)

    @Provides
    @ViewModelScoped
    fun getLocalRecipesFilteredUseCase(
        recipeRepository: RecipeRepository
    ) = GetLocalRecipesFilteredUseCase(recipeRepository)

}