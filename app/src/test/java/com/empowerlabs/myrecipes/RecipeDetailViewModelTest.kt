package com.empowerlabs.myrecipes

import arrow.core.right
import com.empowerlabs.domain.RecipeInfoDomain
import com.empowerlabs.myrecipes.feature.recipeDetail.RecipeDetailViewModel
import com.empowerlabs.usecase.recipe.*
import com.empowerlabs.usecase.recipeDetail.GetLocalRecipeDetailUseCase
import com.empowerlabs.usecase.recipeDetail.GetRemoteRecipeDetailUseCase
import com.empowerlabs.usecase.recipeDetail.SaveRecipeDetailUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runCurrent
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class RecipeDetailViewModelTest {

    @get:Rule
    val coroutinesTestRule = CoroutinesTestRule()

    @Mock
    lateinit var getRemoteRecipeDetailUseCase: GetRemoteRecipeDetailUseCase

    @Mock
    lateinit var getLocalRecipeDetailUseCase: GetLocalRecipeDetailUseCase

    @Mock
    lateinit var saveRecipeDetailUseCase: SaveRecipeDetailUseCase

    private lateinit var recipeDetailViewModel: RecipeDetailViewModel

    @Before
    fun setUp() {
        recipeDetailViewModel = RecipeDetailViewModel(
            getRemoteRecipeDetailUseCase, getLocalRecipeDetailUseCase, saveRecipeDetailUseCase
        )
    }

    @Test
    fun `validate get local recipe detail use case`() = runTest {
        val mockedRecipeDetailDomain = RecipeInfoDomain(1L, "t", "i", "c", "s")
        whenever(getLocalRecipeDetailUseCase(1L)).thenReturn(flowOf(mockedRecipeDetailDomain))

        recipeDetailViewModel.getLocalRecipeDetail(1L)

        val results = mutableListOf<RecipeDetailViewModel.UIState>()
        val job = launch { recipeDetailViewModel.state.toList(results) }
        runCurrent()
        job.cancel()

        assertEquals(
            RecipeDetailViewModel.UIState(localRecipeInfo = mockedRecipeDetailDomain), results[0]
        )
    }

    @Test
    fun `validate get remote recipes use case`() = runTest {
        val mockedRecipeDetailDomain = RecipeInfoDomain(1L, "t", "i", "c", "s")
        whenever(getRemoteRecipeDetailUseCase(1L)).thenReturn(mockedRecipeDetailDomain.right())

        recipeDetailViewModel.getRemoteRecipeDetail(1L)

        val results = mutableListOf<RecipeDetailViewModel.UIState>()
        val job = launch { recipeDetailViewModel.state.toList(results) }
        runCurrent()
        job.cancel()

        assertEquals(
            RecipeDetailViewModel.UIState(remoteRecipeInfo = mockedRecipeDetailDomain), results[0]
        )
    }

}