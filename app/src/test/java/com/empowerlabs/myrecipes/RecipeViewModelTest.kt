package com.empowerlabs.myrecipes

import arrow.core.right
import com.empowerlabs.domain.RecipeDomain
import com.empowerlabs.myrecipes.feature.recipes.RecipeViewModel
import com.empowerlabs.usecase.recipe.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runCurrent
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class RecipeViewModelTest {

    @get:Rule
    val coroutinesTestRule = CoroutinesTestRule()

    @Mock
    lateinit var saveRecipeUseCase: SaveRecipeUseCase

    @Mock
    lateinit var updateRecipeStatusUseCase: UpdateRecipeStatusUseCase

    @Mock
    lateinit var getLocalRecipesUseCase: GetLocalRecipesUseCase

    @Mock
    lateinit var getLocalRecipesFilteredUseCase: GetLocalRecipesFilteredUseCase

    @Mock
    lateinit var getRemoteRecipeUseCase: GetRemoteRecipeUseCase

    private lateinit var recipeViewModel: RecipeViewModel

    @Before
    fun setUp() {
        recipeViewModel = RecipeViewModel(
            saveRecipeUseCase,
            updateRecipeStatusUseCase,
            getLocalRecipesUseCase,
            getLocalRecipesFilteredUseCase,
            getRemoteRecipeUseCase
        )
    }

    @Test
    fun `validate get local recipes use case`() = runTest {
        val mockedRecipeDomain = RecipeDomain(1L, "t", "i", "s", 0)
        val mockedList = listOf(mockedRecipeDomain)
        whenever(getLocalRecipesUseCase()).thenReturn(flowOf(mockedList))

        recipeViewModel.getLocalRecipes()

        val results = mutableListOf<RecipeViewModel.UIState>()
        val job = launch { recipeViewModel.state.toList(results) }
        runCurrent()
        job.cancel()

        assertEquals(RecipeViewModel.UIState(recipes = mockedList), results[0])
    }


    @Test
    fun `validate get local recipes filtered use case`() = runTest {
        val mockedRecipeDomain = RecipeDomain(1L, "t", "i", "s", 0)
        val mockedList = listOf(mockedRecipeDomain)
        whenever(getLocalRecipesFilteredUseCase("test")).thenReturn(flowOf(mockedList))

        recipeViewModel.getRecipesFiltered("test")

        val results = mutableListOf<RecipeViewModel.UIState>()
        val job = launch { recipeViewModel.state.toList(results) }
        runCurrent()
        job.cancel()

        assertEquals(RecipeViewModel.UIState(recipesFiltered = mockedList), results[0])
    }

    @Test
    fun `validate get remote recipes use case`() = runTest {
        val mockedRecipeDomain = RecipeDomain(1L, "t", "i", "s", 0)
        val mockedList = listOf(mockedRecipeDomain)
        whenever(getRemoteRecipeUseCase()).thenReturn(mockedList.right())

        recipeViewModel.getRecipeDataServer()

        val results = mutableListOf<RecipeViewModel.UIState>()
        val job = launch { recipeViewModel.state.toList(results) }
        runCurrent()
        job.cancel()

        assertEquals(RecipeViewModel.UIState(remoteRecipes = mockedList), results[0])
    }

}