package com.empowerlabs.domain

data class RecipeInfoDomain(
    var id: Long, var title: String, var image: String, var creditsText: String, var summary: String
)