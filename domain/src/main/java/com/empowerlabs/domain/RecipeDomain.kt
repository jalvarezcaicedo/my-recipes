package com.empowerlabs.domain

data class RecipeDomain(
    var id: Long, var title: String, var image: String?, var sourceName: String, var favorite: Int
)