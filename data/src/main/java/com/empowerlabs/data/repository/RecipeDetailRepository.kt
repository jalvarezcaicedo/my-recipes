package com.empowerlabs.data.repository

import arrow.core.Either
import com.empowerlabs.data.source.LocalRecipeInfoDataSource
import com.empowerlabs.data.source.RemoteRecipeInfoDataSource
import com.empowerlabs.domain.Error
import com.empowerlabs.domain.RecipeInfoDomain
import kotlinx.coroutines.flow.Flow

class RecipeDetailRepository(
    private val localRecipeInfoDataSource: LocalRecipeInfoDataSource,
    private val remoteRecipeInfoDataSource: RemoteRecipeInfoDataSource
) {

    suspend fun saveRecipeInfoData(recipeInfo: RecipeInfoDomain) {
        localRecipeInfoDataSource.saveRecipeInfo(recipeInfo)
    }

    suspend fun getRecipeData(id: Long): Either<Error, RecipeInfoDomain> {
        return remoteRecipeInfoDataSource.getRecipeInfo(id)
    }

    fun getLocalRecipeInfo(id: Long): Flow<RecipeInfoDomain> =
        localRecipeInfoDataSource.getLocalRecipeInfoData(id)

}