package com.empowerlabs.data.repository

import arrow.core.Either
import com.empowerlabs.data.source.LocalRecipeDataSource
import com.empowerlabs.data.source.RemoteRecipeDataSource
import com.empowerlabs.domain.Error
import com.empowerlabs.domain.RecipeDomain
import kotlinx.coroutines.flow.Flow

class RecipeRepository(
    private val localRecipeDataSource: LocalRecipeDataSource,
    private val remoteRecipeDataSource: RemoteRecipeDataSource
) {

    suspend fun saveRecipesData(recipesData: List<RecipeDomain>) {
        localRecipeDataSource.saveRecipes(recipesData)
    }

    suspend fun updateRecipeStatus(recipeData: RecipeDomain, favorite: Int) {
        localRecipeDataSource.updateRecipeState(recipeData.id.toString(), favorite)
    }

    suspend fun getRecipeData(limit: Long): Either<Error, List<RecipeDomain>> {
        return remoteRecipeDataSource.getRecipes(limit)
    }

    fun getLocalRecipeFiltered(query: String): Flow<List<RecipeDomain>> =
        localRecipeDataSource.getLocalRecipesDataFiltered(query)

    fun getLocalRecipes(): Flow<List<RecipeDomain>> = localRecipeDataSource.getLocalRecipesData()

}