package com.empowerlabs.data.source

import com.empowerlabs.domain.RecipeInfoDomain
import kotlinx.coroutines.flow.Flow

interface LocalRecipeInfoDataSource {

    fun getLocalRecipeInfoData(id: Long): Flow<RecipeInfoDomain>

    suspend fun saveRecipeInfo(recipeInfo: RecipeInfoDomain)

}