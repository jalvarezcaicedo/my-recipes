package com.empowerlabs.data.source

import arrow.core.Either
import com.empowerlabs.domain.Error
import com.empowerlabs.domain.RecipeInfoDomain

interface RemoteRecipeInfoDataSource {

    suspend fun getRecipeInfo(id: Long): Either<Error, RecipeInfoDomain>

}