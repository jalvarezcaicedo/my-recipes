package com.empowerlabs.data.source

import com.empowerlabs.domain.RecipeDomain
import kotlinx.coroutines.flow.Flow

interface LocalRecipeDataSource {
    fun getLocalRecipesData(): Flow<List<RecipeDomain>>
    fun getLocalRecipesDataFiltered(query: String): Flow<List<RecipeDomain>>
    suspend fun saveRecipes(recipes: List<RecipeDomain>)

    suspend fun updateRecipeState(idRecipe: String, favorite: Int)
}