package com.empowerlabs.data.source

import arrow.core.Either
import com.empowerlabs.domain.RecipeDomain

interface RemoteRecipeDataSource {

    suspend fun getRecipes(limit: Long): Either<com.empowerlabs.domain.Error, List<RecipeDomain>>

}