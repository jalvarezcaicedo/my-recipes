package com.empowerlabs.usecase.recipe

import com.empowerlabs.data.repository.RecipeRepository
import com.empowerlabs.domain.RecipeDomain

class SaveRecipeUseCase(
    private val recipeRepository: RecipeRepository
) {

    suspend operator fun invoke(recipes: List<RecipeDomain>) = recipeRepository.saveRecipesData(recipes)

}