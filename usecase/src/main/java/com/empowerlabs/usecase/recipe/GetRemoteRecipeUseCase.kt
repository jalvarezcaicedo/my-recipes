package com.empowerlabs.usecase.recipe

import arrow.core.Either
import com.empowerlabs.data.repository.RecipeRepository
import com.empowerlabs.domain.Error
import com.empowerlabs.domain.RecipeDomain

class GetRemoteRecipeUseCase(
    private val recipeRepository: RecipeRepository
) {
    suspend operator fun invoke(): Either<Error, List<RecipeDomain>> = recipeRepository.getRecipeData(100)
}