package com.empowerlabs.usecase.recipe

import com.empowerlabs.data.repository.RecipeRepository
import com.empowerlabs.domain.RecipeDomain

class UpdateRecipeStatusUseCase(
    private val recipeRepository: RecipeRepository
) {

    suspend operator fun invoke(recipe: RecipeDomain, favorite: Int) =
        recipeRepository.updateRecipeStatus(recipe, favorite)

}