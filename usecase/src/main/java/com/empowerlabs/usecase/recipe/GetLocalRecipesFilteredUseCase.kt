package com.empowerlabs.usecase.recipe

import com.empowerlabs.data.repository.RecipeRepository
import com.empowerlabs.domain.RecipeDomain
import kotlinx.coroutines.flow.Flow

class GetLocalRecipesFilteredUseCase(
    private val recipeRepository: RecipeRepository
) {

    operator fun invoke(query: String): Flow<List<RecipeDomain>> =
        recipeRepository.getLocalRecipeFiltered(query)

}