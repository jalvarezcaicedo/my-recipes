package com.empowerlabs.usecase.recipe

import com.empowerlabs.data.repository.RecipeRepository
import com.empowerlabs.domain.RecipeDomain
import kotlinx.coroutines.flow.Flow

class GetLocalRecipesUseCase(
    private val recipeRepository: RecipeRepository
) {

    operator fun invoke(): Flow<List<RecipeDomain>> = recipeRepository.getLocalRecipes()

}