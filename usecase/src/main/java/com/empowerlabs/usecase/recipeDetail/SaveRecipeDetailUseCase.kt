package com.empowerlabs.usecase.recipeDetail

import com.empowerlabs.data.repository.RecipeDetailRepository
import com.empowerlabs.domain.RecipeInfoDomain

class SaveRecipeDetailUseCase(
    private val recipeDetailRepository: RecipeDetailRepository
) {

    suspend operator fun invoke(recipeInfoDomain: RecipeInfoDomain) =
        recipeDetailRepository.saveRecipeInfoData(recipeInfoDomain)

}