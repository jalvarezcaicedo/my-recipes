package com.empowerlabs.usecase.recipeDetail

import com.empowerlabs.data.repository.RecipeDetailRepository
import com.empowerlabs.domain.RecipeInfoDomain
import kotlinx.coroutines.flow.Flow

class GetLocalRecipeDetailUseCase(
    private val recipeDetailRepository: RecipeDetailRepository
) {

    operator fun invoke(id: Long): Flow<RecipeInfoDomain> =
        recipeDetailRepository.getLocalRecipeInfo(id)

}