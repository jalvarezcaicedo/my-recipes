package com.empowerlabs.usecase.recipeDetail

import arrow.core.Either
import com.empowerlabs.data.repository.RecipeDetailRepository
import com.empowerlabs.domain.Error
import com.empowerlabs.domain.RecipeInfoDomain

class GetRemoteRecipeDetailUseCase(
    private val recipeDetailRepository: RecipeDetailRepository
) {
    suspend operator fun invoke(id: Long): Either<Error, RecipeInfoDomain> =
        recipeDetailRepository.getRecipeData(id)
}